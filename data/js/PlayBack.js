// function show toast message
// function showToasstMessage(message, originClass, replaceClass) {
//   $("#toast-notify").on("show.bs.toast", function () {
//     const toast_header = $("#toast-notify .toast-header");
//     if (toast_header.hasClass(originClass))
//       toast_header.removeClass(originClass).addClass(replaceClass);
//     $("#toast-notify .toast-body").text(message);
//   });
//   $("#toast-notify").toast("show");
// }

// Toast function
function toast({ title = "", message = "", type = "info", duration = 3000 }) {
  const main = document.getElementById("toast__custom");
  if (main) {
    const toast = document.createElement("div");

    // Auto remove toast
    const autoRemoveId = setTimeout(function () {
      main.removeChild(toast);
    }, duration + 1000);

    // Remove toast when clicked
    toast.onclick = function (e) {
      if (e.target.closest(".toast__close")) {
        main.removeChild(toast);
        clearTimeout(autoRemoveId);
      }
    };

    const icons = {
      success: "fas fa-check-circle",
      info: "fas fa-info-circle",
      warning: "fas fa-exclamation-circle",
      error: "fas fa-exclamation-circle",
    };
    const icon = icons[type];
    const delay = (duration / 1000).toFixed(2);
    const timePusher = updateDateTime();

    toast.classList.add("toast__custom", `toast--${type}`);

    toast.style.animation = `slideInLeft ease .3s, fadeOut linear 1s ${delay}s forwards`;
    toast.innerHTML += `
   
                    <div class="toast__icon">
                        <i class="${icon}"></i>
                    </div>
                    <div class="toast__body">
                      <div class="toast__begin">
                        <h3 class="toast__title">${title}</h3>
                        <small class="text-muted"> ${timePusher} </small>
                      </div>
                        <p class="toast__msg">${message}</p>
                    </div>
                    <div class="toast__close">
                        <i class="fas fa-times"></i>
                    </div>
                `;
    main.appendChild(toast);
  }
}

function showSuccessToast(message) {
  toast({
    title: "Success!",
    message: message,
    type: "success",
    duration: 5000,
  });
}

function showErrorToast(message) {
  toast({
    title: "Failure!",
    message: message,
    type: "error",
    duration: 5000,
  });
}
function showWarningToast(message) {
  toast({
    title: "Warning!",
    message: message,
    type: "warning",
    duration: 5000,
  });
}

if (!!window.EventSource) {
  var source = new EventSource("/events");
  source.addEventListener(
    "open",
    function (e) {
      console.log("Events Connected");
    },
    false
  );
  source.addEventListener("error", function (e) {
    if (e.target.readyState != EventSource.OPEN) {
      console.log("Events Disconnected");
    }
  });

  source.addEventListener(
    "addFavor",
    function (e) {
      console.log(e.data);

      // showToasstMessage(e.data, "bg-danger", "bg-success");
      showSuccessToast(e.data);
    },
    false
  );

  source.addEventListener(
    "stopChannel",
    function (e) {
      console.log(e.data);
      // showToasstMessage(e.data, "bg-danger", "bg-success");
      showSuccessToast(e.data);
    },
    false
  );

  source.addEventListener(
    "playChannel",
    function (e) {
      console.log(e.data);
      // showToasstMessage(e.data, "bg-danger", "bg-success");
      showSuccessToast(e.data);
    },
    false
  );

  source.addEventListener(
    "addChannel",
    function (e) {
      // conver json to object
      // showToasstMessage("Add channel success!", "bg-danger", "bg-success");
      showSuccessToast("Add channel success!");
      const myObject = JSON.parse(e.data);
      length_ChannelList += 1;
      console.log(myObject);
      // const myArray = JSON.parse(localStorage.getItem("channelList"));

      if (array === []) {
        localStorage.setItem(
          "channelList",
          "[" + JSON.stringify(myObject) + "]"
        );
      } else {
        array.push(myObject);
        localStorage.setItem("channelList", JSON.stringify(array));
      }
      console.log("🚀 ~ file: PlayBack.js:74 ~ myArray:", array);
      let html =
        '<tr class="tb_tbody_tr" onclick="add(this)" ' +
        'id="tbody_tr_' +
        length_ChannelList +
        '">' +
        '<td id="td-id">' +
        length_ChannelList +
        "</td>" +
        '<td id="td-cName">' +
        myObject["channel_name"] +
        "</td>" +
        '<td id="td-url">' +
        myObject["url"] +
        "</td>" +
        ` <td id="td-action">
        <button
          onclick="showEditModal(this)"
          type="button"
          class="btn btn-info btn-sm"
        >
          <i
            class="fa-regular fa-pen-to-square"
            data-toggle="tooltip"
          ></i>
        </button>
        <button
          type="button"
          class="btn btn-warning btn-sm"
          onclick="showDeleteModal(this)"
        >
          <i
            class="fa-regular fa-trash-can"
            data-toggle="tooltip"
            title="Delete"
          ></i>
        </button>
      </td>` +
        "</tr>";

      $("#table-tbody").append(html);

      $("#toast-notify").toast("show");
    },
    false
  );
}

var current_channel_id_selected = 0;
var current_channel_id_playing = 0;
// function to showEditModal when click button Edit
function showEditModal(element) {
  $('[data-toggle="tooltip"]').tooltip("hide");

  const parentElem = element.parentElement;

  current_channel_id_selected =
    parentElem.parentElement.firstElementChild.textContent;

  $("#modalForEdit").on("show.bs.modal", function () {
    const modal_title = $("#modalForEdit .modal-title");
    modal_title.text("Edit channel with ID: " + current_channel_id_selected);
  });

  $("#modalForEdit").modal();
}
// function to showEditModal when click button Delete
function showDeleteModal(element) {
  $('[data-toggle="tooltip"]').tooltip("hide");

  const parentElem = element.parentElement;

  current_channel_id_selected =
    parentElem.parentElement.firstElementChild.textContent;

  $("#modalForDelete").on("show.bs.modal", function () {
    const modal_title = $("#modalForDelete .modal-title");
    modal_title.text("Delete channel with ID: " + current_channel_id_selected);
  });

  $("#modalForDelete").modal();
}
//Function to add date and time of last update
function updateDateTime() {
  var currentdate = new Date();
  let currentHour =
    currentdate.getHours() < 10
      ? `0${currentdate.getHours()}`
      : currentdate.getHours();
  let currentMinute =
    currentdate.getMinutes() < 10
      ? `0${currentdate.getMinutes()}`
      : currentdate.getMinutes();
  let currentSecond =
    currentdate.getSeconds() < 10
      ? `0${currentdate.getSeconds()}`
      : currentdate.getSeconds();
  var datetime = currentHour + ":" + currentMinute + ":" + currentSecond;
  // document.getElementById("update-time").innerHTML = datetime;
  console.log(datetime);
  return datetime;
}

// Function to add channel name for input channel play
function add(element) {
  const tb_tbody = document.getElementById("table-tbody");

  const tb_tbody_tr = tb_tbody.querySelectorAll(".tb_tbody_tr");
  for (let i = 0; i < tb_tbody_tr.length; i++) {
    const current = document.getElementsByClassName("tbody_tr_active");
    if (current.length > 0) {
      current[0].className = current[0].className.replace(
        "tbody_tr_active",
        ""
      );
    }
  }
  element.classList.add("tbody_tr_active");
  const idChannelPlay = document.getElementById("IdChannelPlay");
  //  channelName = element.querySelector('#td-cName').textContent;
  current_channel_id_selected = element.querySelector("#td-id").textContent;
  // console.log(current_channel_id_selected);
  idChannelPlay.value = current_channel_id_selected;
}

$(document).ready(function () {
  $("form").each(function () {
    let form = $(this);
    form.submit(async function (e) {
      e.preventDefault();
      let actionUrl = form.attr("action");
      let dataToSend = form.serialize();
      let indexChannelEdit = current_channel_id_selected - 1;
      if (actionUrl == "/editChannel") {
        await $("#modalForEdit").modal("hide");
        dataToSend =
          "IndexChannelEdit=" + +indexChannelEdit + "&" + form.serialize();
      }
      console.log(dataToSend);
      $.ajax({
        type: "GET",
        url: actionUrl,
        data: dataToSend, // serializes the form's elements.
        success: async function (data) {
          console.log(data); // show response from the php script.location.reload();
          if (actionUrl == "/editChannel") {
            const object = JSON.parse(data);
            array[indexChannelEdit] = object;
            const queryTdName =
              "#tbody_tr_" + current_channel_id_selected + " #td-cName";
            const queryTdURL =
              "#tbody_tr_" + current_channel_id_selected + " #td-url";
            await $(queryTdName).text(object.channel_name);
            await $(queryTdURL).text(object.url);
            await localStorage.setItem("channelList", JSON.stringify(array));
          }
        },
      });
    });
  });
});

// jquery code for render channel list
var length_ChannelList = 0;
var array = [];
function renderChannelList(length, element) {
  let html = "";
  for (let i = 0; i < length; i++) {
    html +=
      // '<tr onclick="add(this)">' +
      '<tr class="tb_tbody_tr" onclick="add(this)" ' +
      'id="tbody_tr_' +
      `${i + 1}` +
      '">' +
      '<td id="td-id">' +
      `${i + 1}` +
      "</td>" +
      '<td id="td-cName">' +
      array[i].channel_name +
      "</td>" +
      '<td id="td-url">' +
      array[i].url +
      "</td>" +
      ` <td id="td-action">
  <button
    type="button"
    class="btn btn-info btn-sm"
    id="btn_showEditModal"
    onclick="showEditModal(this)"
  >
    <i
      class="fa-regular fa-pen-to-square"
      data-toggle="tooltip"
    ></i>
  </button>
  <button
    type="button"
    class="btn btn-warning btn-sm"
    id="btn_showDeleteModal"
    onclick="showDeleteModal(this)"
  >
    <i
      class="fa-regular fa-trash-can"
      data-toggle="tooltip"
      title="Delete"
    ></i>
  </button>
</td>` +
      "</tr>";
  }
  element.innerHTML += html;
}
$(document).ready(function () {
  // const timeoutID = setTimeout(async () => {
  const channelList = localStorage.getItem("channelList");
  const tb_body = document.getElementById("table-tbody");
  if (tb_body) {
    tb_body.removeChild(tb_body.firstElementChild);
  }

  if (!channelList || channelList == "") {
    $.get("/getChannelList", {}, async function (response) {
      console.log(response || "channel list is empty!");
      if (response != "") {
        array = await JSON.parse(response);
        await localStorage.setItem("channelList", JSON.stringify(array));
        length_ChannelList = array.length;
        console.log(array);
        renderChannelList(length_ChannelList, tb_body);
      }
    });
  } else {
    array = JSON.parse(channelList);
    length_ChannelList = array.length;
    console.log(array);
    if (length_ChannelList > 0) {
      renderChannelList(length_ChannelList, tb_body);
    }
  }

  //  clearTimeout(timeoutID);
  // }, 1000);

  $('[data-toggle="tooltip"]').tooltip();
  $(".btn_nav").click(function () {
    $(".items").toggleClass("show");
    $("ul li").toggleClass("hide");
  });
});

// check when button comfirm delete channel press
$(document).ready(function () {
  $("#btn_confirm_deleteChannel").click(async () => {
    if (current_channel_id_playing === current_channel_id_selected) {
      // showToasstMessage(
      //   "Can't delete a running channel",
      //   "bg-success",
      //   "bg-danger"
      // );
      showErrorToast("Can't delete a running channel");
    } else {
      const indexChannelDelete = current_channel_id_selected - 1;
      await array.splice(indexChannelDelete, 1);
      console.log("btn_confirm_deleteChannel");
      await $.get(
        "/deleteChannel",
        {
          IndexChannelDelete: indexChannelDelete,
          NewArraySplice: JSON.stringify(array),
        },
        function (response) {
          console.log(response);

          localStorage.setItem("channelList", JSON.stringify(array));
          const timeoutID2 = setTimeout(() => {
            location.reload();
            clearTimeout(timeoutID2);
          }, 1000);
        }
      );
    }
  });
});

$(document).ready(function () {
  current_channel_id_playing = localStorage.getItem("idChannelIsRunning");
  console.log(
    "🚀 ~ file: PlayBack.js:85 ~ current_channel_id_playing:",
    current_channel_id_playing
  );
  if (!current_channel_id_playing) {
    current_channel_id_playing = undefined;
  } else {
    const btn_play = $("#btn_play");
    const btn_stop = $("#btn_stop");
    btn_play.prop("disabled", true);
    btn_play.removeClass("btn_play").addClass("btn_disable");
    btn_stop.removeClass("btn_disable").addClass("btn_stop");
    btn_stop.prop("disabled", false);
  }

  $("#btn_play").click(() => {
    current_channel_id_playing = $("#IdChannelPlay").val() || undefined;

    if (current_channel_id_playing != undefined) {
      const btn_play = $("#btn_play");
      const btn_stop = $("#btn_stop");
      btn_play.prop("disabled", true);
      btn_play.removeClass("btn_play").addClass("btn_disable");
      btn_stop.removeClass("btn_disable").addClass("btn_stop");
      btn_stop.prop("disabled", false);
      localStorage.setItem("idChannelIsRunning", current_channel_id_playing);

      const IndexChannelPlay = current_channel_id_playing - 1;

      $.post(
        "/playChannel",
        {
          IndexChannelPlay: IndexChannelPlay,
        },
        function (response) {
          console.log(response);
        }
      );
    } else {
      // $("#notify-json").html("Please add channel want to play");
      // showToasstMessage(
      //   "Please add channel want to play",
      //   "bg-success",
      //   "bg-danger"
      // );
      showErrorToast("Please choose channel want to play");
    }

    // setTimeout(function () {
    //     $('#btn_play').prop('disabled', false);
    // }, 1000);
  });

  $("#btn_addFavor").click(() => {
    $("#btn_addFavor").prop("disabled", true);
    const result = localStorage.getItem("idChannelIsRunning");
    if (result) {
      showWarningToast("Channel ID: " + result + " is running!");
    } else {
      if (
        current_channel_id_playing != undefined &&
        current_channel_id_playing > 0
      ) {
        const ChannelPlayName =
          array[current_channel_id_playing - 1].channel_name;
        $.get(
          "/addFavor",
          {
            ChannelFavor: ChannelPlayName,
          },

          function (response) {
            console.log(response);
          }
        );
      } else {
        //  showToasstMessage("No channels running!!", "bg-success", "bg-danger");
        showErrorToast("No channels running!!");
      }
    }

    const timeout = setTimeout(function () {
      $("#btn_addFavor").prop("disabled", false);
      clearTimeout(timeout);
    }, 1000);
  });

  $("#btn_stop").click(() => {
    const btn_stop = $("#btn_stop");

    btn_stop.removeClass("btn_stop").addClass("btn_disable");
    btn_stop.val("");
    $("#btn_play").prop("disabled", false);
    $("#btn_play").removeClass("btn_disable").addClass("btn_play");
    btn_stop.prop("disabled", true);
    $.post("/stopChannel", {}, function (response) {
      console.log(response);
      current_channel_id_playing = undefined;
      localStorage.removeItem("idChannelIsRunning");
    });
  });

  $("#btn_clear").click(() => {
    console.log("clear form");
    $("#ChannelName").val("");
    $("#ChannelURL").val("");
  });
});
