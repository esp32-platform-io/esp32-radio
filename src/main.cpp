#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
// #include <AsyncElegantOTA.h> // OTA
#include "SPIFFS.h"
#include "../include/EspSPIFFS.h"
#include <Arduino_JSON.h>

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);

// Create an Event Source on /events
AsyncEventSource events("/events");

// Create pointer object for class EspSPIFFS
EspSPIFFS *espSPIFFS = new EspSPIFFS();

// Search for parameter in HTTP POST request
const char *PARAM_INPUT_SSID = "ssid";
const char *PARAM_INPUT_PASS = "pass";
const char *PARAM_INPUT_IP = "ip";
const char *PARAM_INPUT_GATEWAY = "gateway";

// http authentication
const char *http_username = "robotnet";
const char *http_password = "Robotnet@2022";

// Variables to save values from HTML form
String ssid;
String pass;
String ip;
String gateway;

// Set your Local IP address
IPAddress localIP;
// IPAddress localIP(192, 168, 1, 200); // hardcoded

// Set your Gateway IP address
IPAddress localGateway;

// IPAddress localGateway(192, 168, 1, 1); //hardcoded
IPAddress subnet(255, 255, 255, 0);

IPAddress primaryDNS(8, 8, 8, 8);   // optional
IPAddress secondaryDNS(8, 8, 4, 4); // optional

bool initWiFi();

void wifimanager_start();

// function handle when user get unknow router
void notFound(AsyncWebServerRequest *request)
{
  request->send(404, "text/plain", "Not found");
}

// Pram input for requset, example : ?inputURL=<inputMessage>
const char *PARAM_INPUT_ChannelName = "ChannelName";
const char *PARAM_INPUT_ChannelURL = "ChannelURL";
const char *PARAM_INPUT_LineName = "LineName";
const char *PARAM_INPUT_StationName = "StationName";
const char *PARAM_INPUT_ChannelFavor = "ChannelFavor";

const char *PARAM_INPUT_IndexChannelDelete = "IndexChannelDelete";
const char *PARAM_INPUT_IndexChannelEdit = "IndexChannelEdit";
const char *PARAM_INPUT_ChannelEditName = "ChannelEditName";
const char *PARAM_INPUT_ChannelEditURL = "ChannelEditURL";
// declare object for taskhandle
static TaskHandle_t Task1 = NULL;
static TaskHandle_t Task2 = NULL;

// variable use to active wifmanager
bool wifimanager_status = false;

// Globals
// static SemaphoreHandle_t mutex;
int count_retry_connect = 0;
void TaskReconnectWiFi(void *pvParameters)
{

  unsigned long previousMillis_reconnectWifi = 0;
  bool task1_suspend = false;
  while (1)
  {
    // put your main code here, to run repeatedly:
    unsigned long currentMillis = millis();

    // if WiFi is down, tryreconnecting
    if ((WiFi.status() != WL_CONNECTED) && (currentMillis - previousMillis_reconnectWifi >= 5000) && (wifimanager_status == false))
    {

      Serial.print(millis());
      Serial.println(" Reconnecting to WiFi...");

      WiFi.disconnect();
      WiFi.reconnect();
      previousMillis_reconnectWifi = currentMillis;

      count_retry_connect++;

      if (count_retry_connect > 10)
      {

        Serial.println("Unable to reconnect to WiFi -> Start AP again");
        wifimanager_status = true;

        wifimanager_start();
      }
    }

    // Serial.print("HighWaterMark2:");
    // Serial.println(uxTaskGetStackHighWaterMark(NULL));
    // Serial.print("Free heap (bytes): ");
    // Serial.println(xPortGetFreeHeapSize());
    vTaskDelay(100 / portTICK_PERIOD_MS);
  }
  vTaskDelete(NULL);
}

void Wifi_connected(WiFiEvent_t event, WiFiEventInfo_t info)
{
  Serial.println("Successfully connected to Access Point:  " + String(ssid));
  count_retry_connect = 0;
}

void Get_IPAddress_RSSI(WiFiEvent_t event, WiFiEventInfo_t info)
{
  Serial.println("WIFI is connected!");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  long rssi = WiFi.RSSI();
  Serial.print("RSSI:");
  Serial.println(rssi);
}

JSONVar myArray; // preset array
JSONVar favoriteArray;
int _index = 0;
const char input[] = "{\"channel_name\":\"NaN\",\"frequency\":\" \",\"url\":\"NaN\",\"description\":\" \",\"channel_type\":\" \"}";
JSONVar myObject = JSON.parse(input);
int taskMission = 0;
String ChannelName = "";
String ChannelURL = "";
String ChannelFavor = "";
String ChannelPlay = "";
String ChannelPlayURL = ""; // varialble to store Url of channel wirll play
int idChannelPlay = 0;      // varialble to stroe index of channel will play

int indexChannelDelete = 0; // varialble to stroe index of channel will delete
int indexChannelEdit = 0;   // varialble to stroe index of channel will edit
String ChannelEditName = "";
String ChannelEditURL = "";
// Replaces placeholder

String processorPlayback(const String &var)
{

  if (var == "ChannelList")
  {
    String channelList = "Loading...";
    // JSONVar keys = myObject.keys();
    // int size = keys.length();
    // for (int i = 0; i < size; i++)
    // {
    //   const char *key = (const char *)(keys[i]);
    //   const char *value = (const char *)(myObject[keys[i]]);
    //   channelList +=
    //       String("<tr onclick=\"add(this)\">") +
    //       String("<td id=\"td-id\">") + String(i + 1) + String("</td>") +
    //       String("<td id=\"td-cName\">") + key + String("</td>") +
    //       String("<td id=\"td-url\">") + value + String("</td>") +
    //       String('<td id="td-action">') +
    //       String('<i class="fa-regular fa-pen-to-square"></i>') +
    //       String('<i class="fa-regular fa-trash-can"></i>') + String('</td>') +
    //       String("</tr>");
    // }
    Serial.println(channelList);
    return channelList;
  }

  else if (var == "Radio_Response")
  {

    return "hello";
  }
  return String();
}

void Task1code(void *pvParameters)
{
  while (1)
  {
    switch (taskMission)
    {
    case 1: // Add channel
    {
      Serial.println("addChannel");
      myObject["channel_name"] = ChannelName;
      myObject["url"] = ChannelURL;
      myArray[_index] = myObject;
      String message = JSON.stringify(myObject);
      Serial.println(message);
      // Store data to SPIFFS in file channel.json
      espSPIFFS->writeFile(SPIFFS, "/channel.json", JSON.stringify(myArray).c_str());
      events.send(message.c_str(), "addChannel", millis());
      vTaskDelay(10 / portTICK_PERIOD_MS);
      _index++;

      taskMission = 0;

      break;
    }

    case 2: // Add favorite channel
    {
      String message = "Add favorite channel done";
      events.send(message.c_str(), "addFavor", millis());
      vTaskDelay(100 / portTICK_PERIOD_MS);
      taskMission = 0;
      break;
    }
    case 3: // Play channel
    {
      String message = ChannelPlay + " will be played!";
      Serial.println("message: " + message);
      events.send(message.c_str(), "playChannel", millis());
      vTaskDelay(100 / portTICK_PERIOD_MS);
      taskMission = 0;
      break;
    }
    case 4: // Stop channel
    {
      String message = "Play channel end";
      events.send(message.c_str(), "stopChannel", millis());
      vTaskDelay(100 / portTICK_PERIOD_MS);
      taskMission = 0;
      break;
    }
    case 5: // Edit channel
    {
      String message = "Edit channel success!";
      myObject["channel_name"] = ChannelEditName;
      myObject["url"] = ChannelEditURL;
      myArray[indexChannelEdit] = myObject;
      // Store data to SPIFFS in file channel.json
      espSPIFFS->writeFile(SPIFFS, "/channel.json", JSON.stringify(myArray).c_str());
      events.send(message.c_str(), "stopChannel", millis());
      vTaskDelay(100 / portTICK_PERIOD_MS);
      taskMission = 0;
      break;
    }
    case 6: // Delete channel
    {
      String message = "Delete channel success! Page will reload!";
      int lengthOriginArray = myArray.length();

      int indexForNewArraySplice = 0;
      Serial.println("My array after delete channel: " + myArray);
      // Store data to SPIFFS in file channel.json
      espSPIFFS->writeFile(SPIFFS, "/channel.json", JSON.stringify(myArray).c_str());
      events.send(message.c_str(), "stopChannel", millis());
      vTaskDelay(100 / portTICK_PERIOD_MS);
      taskMission = 0;
      break;
    }
    default:
    {
      break;
    }
    }
    vTaskDelay(200 / portTICK_PERIOD_MS);
  }
}
void setup()
{
  // put your setup code here, to run once:
  Serial.begin(115200);

  // delete old config
  WiFi.disconnect(true);
  WiFi.onEvent(Wifi_connected, ARDUINO_EVENT_WIFI_STA_CONNECTED);
  WiFi.onEvent(Get_IPAddress_RSSI, ARDUINO_EVENT_WIFI_STA_GOT_IP);

  vTaskDelay(2000 / portTICK_PERIOD_MS);

  espSPIFFS->initSPIFFS();

  // Read field ssid, pass, ip , gateway
  ssid = espSPIFFS->readSSID(SPIFFS);
  pass = espSPIFFS->readPASS(SPIFFS);
  ip = espSPIFFS->readIP(SPIFFS);
  gateway = espSPIFFS->readGATEWAY(SPIFFS);

  myArray = JSON.parse(espSPIFFS->readFile(SPIFFS, "/channel.json"));
  _index = myArray.length();
  if (_index == -1)
  {
    _index = 0;
  }

  Serial.println(myArray);

  Serial.println(ssid);
  Serial.println(pass);
  Serial.println(ip);
  Serial.println(gateway);

  // Create mutex before starting tasks
  // mutex = xSemaphoreCreateMutex();
  if (initWiFi())
  {
    // Handle the Web Server in Station Mode
    //  Route for root / web page
    server.serveStatic("/", SPIFFS, "/");

    server.on("/PlayBack", HTTP_GET, [](AsyncWebServerRequest *request)
              {   
                // if(!request->authenticate(http_username, http_password))
                //     return request->requestAuthentication();
                request->send(SPIFFS, "/views/PlayBack.html", "text/html", false, processorPlayback); });
    server.on("/getChannelList", HTTP_GET, [](AsyncWebServerRequest *request)
              {   
                // if(!request->authenticate(http_username, http_password))
                //     return request->requestAuthentication();
                 request->send(200, "text/html", JSON.stringify(myArray).c_str()); });

    server.on("/addChannel", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                int params = request->params();
                for (int i = 0; i < params; i++)
                {
                  AsyncWebParameter *p = request->getParam(i);
                  
                  
                    if (p->name() == PARAM_INPUT_ChannelName)
                    {
                      ChannelName = p->value();
                    }
                    if (p->name() == PARAM_INPUT_ChannelURL)
                    {
                      ChannelURL = p->value();
                    }
                  
                }
                taskMission = 1;
                Serial.print("Add new channel: "+ ChannelName);
                Serial.println("with url: " + ChannelURL);

                request->send(200, "text/html", "add channel success"); });

    server.on("/addFavor", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                AsyncWebParameter *p = request->getParam(PARAM_INPUT_ChannelFavor);
                  
                ChannelFavor = p->value();
                taskMission = 2;
                  
                  
                Serial.println("addFavor: " + String(ChannelFavor));
                request->send(200, "text/html", "Send Channel Favor"); });
    server.on("/playChannel", HTTP_POST, [](AsyncWebServerRequest *request)
              {
                int params = request->params();
      for (int i = 0; i < params; i++)
      {
        AsyncWebParameter *p = request->getParam(i);
        if (p->isPost())
        {
          idChannelPlay = p->value().toInt();                  
          ChannelPlay = myArray[idChannelPlay]["channel_name"];
          ChannelPlayURL = myArray[idChannelPlay]["url"];
          Serial.println("play channel: " + ChannelPlay + " with url: " + ChannelPlayURL);
        }
      }
      taskMission = 3;     
      request->send(200, "text/html", "Send Play Channel"); });

    server.on("/stopChannel", HTTP_POST, [](AsyncWebServerRequest *request)
              {            
                taskMission = 4;     
                Serial.println("stop channel");
                request->send(200, "text/html", "Send Stop Channel"); });
    server.on("/editChannel", HTTP_GET, [](AsyncWebServerRequest *request)
              {   
               int params = request->params();
                for (int i = 0; i < params; i++)
                {
                  AsyncWebParameter *p = request->getParam(i);
                  
                  
                    if (p->name() == PARAM_INPUT_IndexChannelEdit)
                    {
                      indexChannelEdit = p->value().toInt();
                      Serial.println("indexChannelEdit: " + String(indexChannelEdit));
                    }
                    if (p->name() == PARAM_INPUT_ChannelEditName)
                    {
                      ChannelEditName = p->value();
                    }
                     if (p->name() == PARAM_INPUT_ChannelEditURL)
                    {
                      ChannelEditURL = p->value();
                    }
                }
                 myObject["channel_name"] = ChannelEditName;
                 myObject["url"] = ChannelEditURL;
                  
            
                 taskMission = 5;
                 request->send(200, "text/html", JSON.stringify(myObject)); });
    server.on("/deleteChannel", HTTP_GET, [](AsyncWebServerRequest *request)
              {   
                 int params = request->params();
                for (int i = 0; i < params; i++)
                {
                  AsyncWebParameter *p = request->getParam(i);
                  
                  
                    if (p->name() == PARAM_INPUT_IndexChannelDelete)
                    {
                        indexChannelDelete = p->value().toInt();
                    }
                    if(p->name()=="NewArraySplice")
                    {
                      String newArraySplice = p->value();
                      myArray = JSON.parse(newArraySplice);
                      // Serial.println("newArraySplice: "+newArraySplice);
                    }
                  
                }
                 Serial.println("indexChannelDelete: " + String(indexChannelDelete));
                 taskMission = 6;
                 request->send(200, "text/html", "Delete channel success!"); });

    server.onNotFound(notFound);

    events.onConnect([](AsyncEventSourceClient *client)
                     {
    if(client->lastId()){
      Serial.printf("Client reconnected! Last message ID that it got is: %u\n", client->lastId());
                } });

    // Start ElegantOTA
    // AsyncElegantOTA.begin(&server, http_username, http_password);

    server.addHandler(&events);
    server.begin();
  }
  else
  {

    wifimanager_status = true;
    wifimanager_start();
  }

  // xTaskCreatePinnedToCore(
  //     TaskReconnectWiFi,   /* Task function. */
  //     "TaskReconnectWiFi", /* name of task. */
  //     1024 * 4,            /* Stack size of task (byte in ESP32) */
  //     NULL,                /* parameter of the task */
  //     2,                   /* priority of the task */
  //     NULL,                /* Task handle */
  //     0);                  /* Run on one core*/
  xTaskCreatePinnedToCore(
      Task1code, /* Task function. */
      "Task1",   /* name of task. */
      2048 * 2,  /* Stack size of task (byte in ESP32) */
      NULL,      /* parameter of the task */
      3,         /* priority of the task */
      &Task1,    /* Task handle */
      1);        /* Run on one core*/
  // Show that we accomplished our task of passing the stack-based argument
  Serial.println("Done!");
}

void loop()
{
}

unsigned long previousMillis_cnWifi = 0;
bool initWiFi()
{
  // interval to wait for Wi-Fi connection (milliseconds)
  const long interval = 10000;

  // Check ssid or ip exists
  if (ssid == "" || ip == "")
  {
    Serial.println("Undefined SSID or IP address.");
    return false;
  }

  WiFi.mode(WIFI_STA);

  // // -------------- Config ESP Wifi_sta with ip static--------
  localIP.fromString(ip.c_str());
  localGateway.fromString(gateway.c_str());

  if (!WiFi.config(localIP, localGateway, subnet, primaryDNS, secondaryDNS))
  {
    Serial.println("STA Failed to configure");
    return false;
  }

  WiFi.begin(ssid.c_str(), pass.c_str());
  Serial.println("Connecting to WiFi...");

  unsigned long currentMillis = millis();
  previousMillis_cnWifi = currentMillis;

  while (WiFi.status() != WL_CONNECTED)
  {
    currentMillis = millis();
    if (currentMillis - previousMillis_cnWifi >= interval)
    {
      Serial.println("Failed to connect.");
      return false;
    }
  }

  return true;
}
void wifimanager_start()
{

  // Connect to Wi-Fi network with SSID and password
  Serial.println("Setting AP (Access Point)");
  // NULL sets an open Access Point
  WiFi.softAP("ESP-WIFI-MANAGER", "22446688");

  IPAddress IP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(IP);
  server.serveStatic("/", SPIFFS, "/");
  // Web Server Root URL
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
            { request->send(SPIFFS, "/views/wifimanager.html", "text/html"); });

  server.on("/", HTTP_POST, [](AsyncWebServerRequest *request)
            {
      int params = request->params();
      for(int i=0;i<params;i++){
        AsyncWebParameter* p = request->getParam(i);
        if(p->isPost()){
          // HTTP POST ssid value
          if (p->name() == PARAM_INPUT_SSID) {
            ssid = p->value().c_str();
            Serial.print("SSID set to: ");
            Serial.println(ssid);
            // Write file to save value
            espSPIFFS->writeSSID(SPIFFS,ssid.c_str());
          }
          // HTTP POST password value
          if (p->name() == PARAM_INPUT_PASS) {
            pass = p->value().c_str();
            Serial.print("Password set to: ");
            Serial.println(pass);
            // Write file to save password
            espSPIFFS->writePASS(SPIFFS, pass.c_str());
          }
          // HTTP POST ip value
          if (p->name() == PARAM_INPUT_IP) {
            ip = p->value().c_str();
            Serial.print("IP Address set to: ");
            Serial.println(ip);
            // Write file to save value
            espSPIFFS->writeIP(SPIFFS, ip.c_str()); 
          }
          // HTTP POST gateway value
          if (p->name() == PARAM_INPUT_GATEWAY) {
            gateway = p->value().c_str();
            Serial.print("Gateway set to: ");
            Serial.println(gateway);
            // Write file to save value
            espSPIFFS->writeGATEWAY(SPIFFS, gateway.c_str());
          }
          //Serial.printf("POST[%s]: %s\n", p->name().c_str(), p->value().c_str());
        }
      }
      request->send(200, "text/plain", "Done. ESP will restart, connect to your router and go to IP address: " + ip);
      //wifimanager_status = false;
      delay(3000);
      ESP.restart(); });
  server.begin();
}
